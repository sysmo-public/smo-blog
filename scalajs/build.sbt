name := "sysmo-web-blog"

val v = new {
  val app = "1.0"
  val scala = "2.13.1"
  val scalaJSDom = "1.1.0"
  val scalaJSReact = "1.7.7"
  val scalaCss = "0.6.1"
  val reactJS = "16.8.6"
}

version := v.app
scalaVersion := v.scala



libraryDependencies ++= Seq(
  "org.scala-js" %%% "scalajs-dom" % v.scalaJSDom,
  "com.github.japgolly.scalajs-react" %%% "core" % v.scalaJSReact,
  "com.github.japgolly.scalajs-react" %%% "extra" % v.scalaJSReact,
  "com.github.japgolly.scalacss" %%% "core" % v.scalaCss,
  "com.github.japgolly.scalacss" %%% "ext-react" % v.scalaCss
)

enablePlugins(ScalaJSPlugin)
(scalaJSUseMainModuleInitializer in Compile):= true

// creates single js resource file for easy integration in html page

enablePlugins(WebScalaJSBundlerPlugin)


npmDependencies in Compile ++= Seq(
  "react" -> "16.13.1",
  "react-dom" -> "16.13.1"
)

// Live Reloading: WorkbenchPlugin must NOT be enabled at the same time
//enablePlugins(WorkbenchSplicePlugin)
//workbenchCompression := true
//workbenchStartMode := WorkbenchStartModes.OnCompile